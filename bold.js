const config = require('./boldTest_inject').config; 
const process = require('process')

let arg = process.argv[2]; 
let index    = './www/index.html'
let srcIndex = './src/index.html'

const err = function () {
    console.log('node node-modules/bold-test/bold.js -src or -www or -h or -v')    
}
const help = function () {
    console.log(`    
    node node-modules/bold-test/bold.js -src : inject ./src/index.html with test files
    node node-modules/bold-test/bold.js -www : inject ./www/index.html with test files
    node node-modules/bold-test/bold.js -h   : displays this help message
    node node-modules/bold-test/bold.js -v   : displays application version
    `)
}

const version = function(){
    console.log('version 0.0.1')
}

switch (arg) {
    case '-src':
        config(srcIndex);
        break; 
    case '-www':
        config(index);
        break; 
    case '-h':
        help();
        break; 
    case '-v':
        version();
        break; 
    default: 
        err()  
}