"use strict";
/*    
------------------------------------------------------------------------------------       
--------------- bold-test global variable declaration-------------------------------                 
*/

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var it = null;
var beforeStart = null;
var afterDoing = null;
var whenDone = null;
var beforeDoing = null;
var $testing = {};
$testing.title = null;
$testing.tests = {};
$testing.tests.testFunctions = {};
$testing.reporter = {};
var $report = $testing.reporter.default = {};
var beforeStartObj = $testing.tests.testFunctions['beforeStart'] = [];
var whenDoneObj = $testing.tests.testFunctions['whenDone'] = [];

/* 
------------------------------------------------------------------------------------       
------------------------------------------------------------------------------------                    
*/

/*    
------------------------------------------------------------------------------------       
----------------------- bold-test TEST BUILDER--------------------------------------                 
*/
/**
 * The bt() function captures all user defined test functions, and pushes them into an
 * organized test object ```$tests```. The function runs with each bt() call from within 
 * user tests. The test object has it own global namespace allowing retention of data 
 * between tests.  
 * @param {string} title
 * @exports $testing
 * @namespace bt
 */
var bt = function bt(title) {
    //Test Name Space
    var test = $testing.tests[title] = {};
    var beforeDoingObj = test['beforeDoing'] = [];
    var actionObj = test['it'] = {};
    var afterDoingObj = test['afterDoing'] = [];

    /**
     * Pushes passed function into passed object 
     * @memberof bt
     * @method pushTestFunction
     */
    var pushTestFunction = function pushTestFunction() {
        var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
        var arry = arguments[1];

        if (obj && arry.length > 0) {
            arry.forEach(function (fn) {
                if (typeof fn == 'function') {
                    obj.push(fn);
                }
            });
        }
    };

    /**
     * The it() function wll capture passed function and string designation for 
     * the test action being performed, and add it to $testing.tests[title]['it']
     * @memberof bt
     * @method it
     */
    it = function action() {
        var args = [].slice.call(arguments);
        actionObj[args[0]] = args[1];
    };
    /**
     * The beforeStart() function wll capture passed functions and push them to 
     * $testing.tests.testFunctions['beforeStart']. 
     * @memberof bt
     * @method beforeStart
     */
    beforeStart = function beforeStart() {
        var args = [].slice.call(arguments);
        pushTestFunction(beforeStartObj, args);
    };
    /**
     * The beforeDoing() function wll capture passed functions and push them to 
     * $testing.tests[title]['beforeDoing']
     * @memberof bt
     * @method beforeDoing
     */
    beforeDoing = function beforeDoing() {
        var args = [].slice.call(arguments);
        pushTestFunction(beforeDoingObj, args);
    };
    /**
     * The whenDone() function wll capture passed functions and push them to 
     * $testing.tests.testFunctions['whenDone'] 
     * @memberof bt
     * @method whenDone
     */
    whenDone = function whenDone() {
        var args = [].slice.call(arguments);
        pushTestFunction(whenDoneObj, args);
    };
    /**
     * The afterDoing() function wll capture passed functions and push them to 
     * $testing.tests[title]['afterDoing']
     * @memberof bt
     * @method aferDoing
     */
    afterDoing = function afterDoing() {
        var args = [].slice.call(arguments);
        pushTestFunction(afterDoingObj, args);
    };

    /**
     * This code will call all functions passed to the bt() function. Building
     * the required $testing object. 
     */
    var args = [].slice.call(arguments);
    args.forEach(function (fn) {
        if (typeof fn == 'function') {
            fn();
        }
    });
};
/* 
------------------------------------------------------------------------------------       
------------------------------------------------------------------------------------                    
*/

/*    
------------------------------------------------------------------------------------       
------------------------- bold-test REPORTER ---------------------------------------                 
*/

/**
 * The bold-test reporter will console the testing results and provide an alert 
 * indicating the the number of passed and failed tests. 
 * @namespace $report
 */

$report.makeAlert = function (pass, fail) {
    var msg = "Bold-Test Results:" + "\n" + "passed: " + pass + "\nfailed: " + fail;
    alert(msg);
};
/**
 * This function will console log all passed tests
 * @param {object} results 
 * @memberof $report
 * @method pass 
 */
$report.pass = function pass(test, msg) {
    console.log("%c" + '✔ ' + test + ' ' + msg, "font-weight:bold; color: Green");
};
/**
 * This function will console log all failed tests
 * @param {object} results 
 * @memberof $report
 * @method fail 
 */
$report.fail = function (test, err) {
    console.log("%c" + '✘ ' + test, "font-weight:bold; color: Red");
    console.log("%c" + err, "color: black");
};
/**
 * This function will console log the test name passed to the bt function
 * @param {object} results
 * @memberof $report
 * @method testName 
 */
$report.testName = async function () {
    console.log('***************************************');
    console.log("%c" + 'Bold-Test Report', "font-weight:bold; color: Black; font-size: 16px;");
    console.log('***************************************');
};
$report.makeReport = async function (pass, passNum, fail, failNum) {
    await pass.forEach(function (test) {
        var x = Object.keys(test);
        $report.pass(x[0], test[x[0]]);
    });
    await fail.forEach(function (test) {
        var x = Object.keys(test);
        $report.fail(x[0], test[x[0]]);
    });
    $report.makeAlert(passNum, failNum);
};
/*
-------------------------------------------------------------------------------------       
-------------------------------------------------------------------------------------          
*/

/*    
------------------------------------------------------------------------------------       
------------------------ bold-test TEST RUNNER -------------------------------------                 
*/
//write passed tests to local storage
$testing.pass = function (name, fn) {
    var array1 = JSON.parse(localStorage.getItem('bold-test-pass'));
    var x1 = {};
    x1[name] = fn;
    array1.push(x1);
    localStorage.setItem('bold-test-pass', JSON.stringify(array1));
};
//write failed tests to local storage
$testing.fail = function (name, fn, err) {
    var array2 = JSON.parse(localStorage.getItem('bold-test-fail'));
    var x2 = {};
    x2[name + ': ' + fn] = err.message;
    array2.push(x2);
    localStorage.setItem('bold-test-fail', JSON.stringify(array2));
};
//write passed tests count to local storage
$testing.countPass = function () {
    if (!localStorage.getItem('bold-test-pass-num')) {
        localStorage.setItem('bold-test-pass-num', 0);
    }
    localStorage.setItem('bold-test-pass-num', parseInt(localStorage.getItem('bold-test-pass-num')) + 1);
};
//write failed tests count to local storage
$testing.countFail = function () {
    if (!localStorage.getItem('bold-test-fail-num')) {
        localStorage.setItem('bold-test-fail-num', 0);
    }
    localStorage.setItem('bold-test-fail-num', parseInt(localStorage.getItem('bold-test-fail-num')) + 1);
};
//add current test function to local storage array
$testing.sequence = function (name, fn) {
    //push fn to local storage array
    var x = JSON.parse(localStorage.getItem('bold-test'));
    x.push(name + '.' + fn);
    localStorage.setItem('bold-test', JSON.stringify(x));
};
//initilize local storage key value pair
$testing.initLocalStore = function () {
    if (!localStorage.getItem('bold-test')) {
        localStorage.setItem('bold-test', JSON.stringify([]));
    }

    if (!localStorage.getItem('bold-test-fail')) {
        localStorage.setItem('bold-test-fail', JSON.stringify([]));
    }
    if (!localStorage.getItem('bold-test-pass')) {
        localStorage.setItem('bold-test-pass', JSON.stringify([]));
    }
};
/**
 * The bold-test runner will execute test and record the results
 * @namespace $testing
 */
//run before, beforeDoing, afterDoing, and whenDone functions
$testing.runBeforeAfter = function (fns) {
    if ((typeof fns === 'undefined' ? 'undefined' : _typeof(fns)) == 'object' && fns.length > 0) {
        fns.forEach(function (fn) {
            fn();
        });
    }
};
/**
 * The runIt() function runs all passed functions records any error
 * and pushes the results to $testing.results[name]
 * @param {string} name string designation of current test
 * @param {object} fns 
 */
$testing.runIt = async function (name, fns) {

    var arry = Object.keys(fns);
    $testing.initLocalStore();
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
        for (var _iterator = arry[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var fn = _step.value;

            //get local storage reference to executed functions
            var x = JSON.parse(localStorage.getItem('bold-test'));
            if (!(x.indexOf(name + '.' + fn) > -1)) {
                //update local storage with executed functions
                $testing.sequence(name, fn);
                try {
                    await fns[fn]();
                    //increment passed counter
                    $testing.countPass();
                    $testing.pass(name, fn);
                } catch (err) {
                    //increment fail counter
                    $testing.countFail();
                    $testing.fail(name, fn, err);
                }
            }
        }
    } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
            }
        } finally {
            if (_didIteratorError) {
                throw _iteratorError;
            }
        }
    }
};
/**
 * The runtest() function executes the built $testing object tests in the 
 * orderd required, captures the test results, and passes them to the
 * reporting function upon completion. 
 */
$testing.runTest = async function () {
    var fns = $testing.tests.testFunctions;
    await this.runBeforeAfter(fns['beforeStart']);
    var arry = Object.keys($testing.tests);
    var _iteratorNormalCompletion2 = true;
    var _didIteratorError2 = false;
    var _iteratorError2 = undefined;

    try {
        for (var _iterator2 = arry[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
            var test = _step2.value;

            if (test !== 'testFunctions') {
                var exec = this.tests[test];
                await this.runBeforeAfter(exec['beforeDoing']);
                await this.runIt(test, exec['it']);
                await this.runBeforeAfter(exec['afterDoing']);
            }
        }
    } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion2 && _iterator2.return) {
                _iterator2.return();
            }
        } finally {
            if (_didIteratorError2) {
                throw _iteratorError2;
            }
        }
    }

    await this.runBeforeAfter(fns['whenDone']);
    var pass = JSON.parse(localStorage.getItem('bold-test-pass'));
    var passNum = localStorage.getItem('bold-test-pass-num');
    var fail = JSON.parse(localStorage.getItem('bold-test-fail'));
    var failNum = localStorage.getItem('bold-test-fail-num');
    await $report.testName();
    await $report.makeReport(pass, passNum, fail, failNum);
    $testing.done();
};

//Give the app 5 seconds to be ready, then start the test.
setTimeout(async function () {
    $testing.runTest();
}, 5000);
$testing.done = function () {
    //Give the app 5 seconds to realize testing is finished and garbage collect.
    setTimeout(async function () {
        localStorage.removeItem('bold-test');
        localStorage.removeItem('test');
        localStorage.removeItem('bold-test-pass');
        localStorage.removeItem('bold-test-fail');
        localStorage.removeItem('bold-test-pass-num');
        localStorage.removeItem('bold-test-fail-num');
    }, 5000);
};
/*
----------------------------------------------------------------------------------------         
----------------------------------------------------------------------------------------                
*/