'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

if (!bt) {
    throw new Error('bt is undefined. Include boldTest_bootstrap.js');
}

///////////////////////////////////////////////
/// Test Management Methods 
/// 
var $bt;
$bt = function $bt() {
    this.element = '';
    this.elements = [];
};
/**
 * Waits given amount of milliseconds before proceeding.
 * @param  {number} ms Amount of milliseconds to wait for.
 * @return {Promise}    
 */
$bt.wait = async function (ms) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            return resolve('Waited for ' + ms + 'ms.');
        }, ms);
    });
};
///////////////////////////////////////////////
/// Page Access Methods 
/// 
/**
 * Returns the current page name, as identified by the url hash.
 * @return {string}
 */
$bt.page = function () {
    return document.location.hash.substr(2);
};
///////////////////////////////////////////////
/// Element Access Methods 
/// 
/**
 * Get's the first element that matches the css-selector string.
 * @param  {string} query A standard CSS selector string.
 * @return {Element}      The selected Element or null.
 */
$bt.get = function (query) {
    this.element = document.querySelector(query);
    return this;
};
/**
 * Get's all elements that matches the css-selector string.
 * @param  {string} query A standard CSS selector string.
 * @return {Element}      The selected Elements or null.
 */
$bt.getAll = function (query) {
    this.elements = document.querySelectorAll(query);
    return this;
};
/**
 * Simulates a click event on an element.
 * @param  {Element} The element to click on.
 */
$bt.click = function () {
    this.element.dispatchEvent(new TouchEvent('touchstart', { bubbles: true }));
    this.element.click();
    return this;
};
/**
 * Simulates an input event, and sets the value on the input.
 * @param {Element} element The element to set the value on.
 */
$bt.setValue = function (value) {
    this.element.dispatchEvent(new FocusEvent('focus'));
    this.element.value = value;
    this.element.dispatchEvent(new InputEvent('input'));
    this.element.dispatchEvent(new FocusEvent('blur'));
    return this;
};
/**
 * Clicks on element within an array of objects formed using provided query that has the designated innerText value provided
 * @param {String} text - innerText value
 * @param {String} query - css selector query for elements
 */
$bt.clickHasText = async function (text, query) {
    $bt.getAll(query);
    await $bt.wait(1000);
    if (_typeof($bt.elements) == 'object') {
        $bt.elements.forEach(function (element) {
            var x = element.innerText.trim();
            if (x == text) {
                element.click();
            }
        });
    }
};

///////////////////////////////////////////////
/// bt Internal
/// 

// Better to start tests with a custom event emitted from app, when cordova + app is ready.
// document.addEventListener("ionicPlatformReady", function(event) {
//     bt._run();
// });