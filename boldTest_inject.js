
//Injects the e2e tests into the build.
let fs = require('fs');
var exec = require('child_process').exec;

//file and directory paths
let workingDir      = ['./www/bt-e2e/', './www/bt-e2e/lib']
let corfiles        = './node_modules/bold-test/lib'
let corfilesPath    = './node_modules/bold-test/lib/'
let tests           = './tests'
let testsPath       = './tests/'
let script          = './www/bt-e2e/lib/bold-test.js'
let scriptPath      = 'bt-e2e/lib/bold-test.js'

//Create directory at specified path
const makeDir = async function (path) {
    if (!fs.existsSync(path)) {
        fs.mkdir(path, (err) => {
            if (err) {
                console.log(err)
            }
        })
    }
}
//Concatonates two files
const concatFile = async function (wr, re) {
    var w = fs.createWriteStream(wr, { flags: 'a' })
    var r = fs.createReadStream(re)
    r.pipe(w)
}
//delete ./www/bt-e2e/bold-test.js if exists
const deleteScript = async function () {
    const path = script; 
    if (fs.existsSync(path)){
        await fs.unlink(path, (err) => {
            if(err) console.log(err)
        })
    }
}
/**
 * Copy file from one directory to other
 * @param {string} src copy from directory
 * @param {string} dest copy to directory
 */
const copy = function (src, dest) {
    fs.copyFileSync(src , dest); 
}
/**
 * Injects ./www/index.html with provided string, if does not exist already
 * @param {string} string <script src="./www/bt-e2e/lib/bold-test.js"></script>
 */
const inject = async function (url, string) {
    let index = fs.readFileSync(url, 'utf8')
        if (!index.match(`<script src="${scriptPath}"></script>`)) {
            fs.writeFileSync(url, string + '\n' , {flag:'a'});  
        }   
}
const deleteMe = async function (path) {
    if (fs.existsSync(path)){
        fs.unlinkSync(path)
    };
}
//concat test files by number sequence  1_fileName to 100_fileName --> testfiles.js
const concatTests = async function () {
    exec("cat ./tests/{0..100}_*.js > ./tests/testfiles.js 2>> /dev/null", function(err, stdout, stderr) {
        if (stderr) {
            console.log(stderr)
        } else {
            concatFile(script, './tests/testfiles.js')
        }
    });
} 
exports.config = async function (srcDirectory) {
    //delete ./www/bt-e2e/lib/bold-test.js if exists
    await deleteScript()
    //creat app working directories
    await makeDir('./www/bt-e2e')
    await makeDir('./www/bt-e2e/lib')
    // concat core files
    await concatFile(script, './node_modules/bold-test/lib/1_boldTest_bootstrap.js')
    await concatFile(script,'./node_modules/bold-test/lib/_should.min.js')
    await concatFile(script, './node_modules/bold-test/lib/2_boldTest_run.js')
    //concatonate test files and inject into ./www/bt-e2e/bold-test.js
    await deleteMe('./tests/testfiles.js')
    await concatTests()
    await inject(srcDirectory, `<script src="${scriptPath}"></script>`)

}
