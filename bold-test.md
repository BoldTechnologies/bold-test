![logo](http://static1.squarespace.com/static/520ea5b3e4b0e8144a5d9dc2/t/5a9c44a09140b7602da47094/1525719977844/?format=10w)
# bold-test
This package will enable users to write hybrid application tests and inject them directly into your projects HTML. The tests will run after project is opened in a browser, or emulator. A report will be provided in the console. 


## TOC:
- [Notes](#notes)
- [Getting Started](#getting-started)
    * [Installation](#installation)
    * [bold-test script](#bold-test-script)
    * [Create a test](#create-a-test)
    * [In the Browser](#in-the-browser)
    * [ In iOS Emulator](#in-ios-emulator)
- [Test Functions](#test-functions)
    * [wait](#wait)
    * [page](#page)
    * [get](#get)
    * [getAll](#getAll)
    * [click](#click)
    * [setValue](#setValue)
## Notes: 
1. Due to a bug with ionic, delay times are require between tasks. For example setting the value on an input form and then clicking on the submit button, a $bt.wait(some time) should be added prior to clicking on the submit button.  

## Getting Started
### Installation 
```bash
> npm install --save-dev git+'https://bitbucket.org/BoldTechnologies/bold-test.git'
```

### bold-test script
Running the provided npm script bt-e2e will take bold-test core files and concatonate them with your test files, and then place them in your projects ./www/bt-e2e/lib directory. The project index.html file located in ./www/index.html will be injected with a src link pointing at that file. 
```json
  "scripts": {
        "bt-e2e": "node node_modules/bold-test/bold.js -t"
  }
```

### Create a test
Write tests and place them in the **./tests** directory. Sample test is below. 
```javascript
bt('Example Test', async function () {
   
    it('Should arrive at the login page', function () {
        let url =  $bt.page(); 
        url.should.equal('login')
    })

    it('Should recieve an alert when logging in with invalid credentials', async function () {
        $bt
            .get('#login-email input')
            .setValue('invalidEmail@invalid.com')
            .get('#login-password input')
            .setValue('12345678Aa')

        await bt.wait(1000);
            
        $bt
            .get('#login-submit').click()

        await bt.wait(2000)
           
            
        $bt
            .get('ion-alert .alert-title')
            .element.innerText.should.be.equal('Credentials Incorrect')

        $bt
            .get('ion-alert .alert-button').click()
    })
});
```

### In the browser

1. ```> ionic serve ```
2. ```> npm run bt-e2e```
3. refresh browser and test will run


### In iOS Emulator

1. ```> ionic build```
2. ```> npm run bt-e2e```
3. ```> ionic cordova build ios ```
3. Run the generated project via Xcode (ie: open ./platforms/ios/retailerapp-v2.xcworkspace/)

* * *
# Test Functions
* * *

## wait
```$bt.wait(ms)```

Waits given amount of milliseconds before proceeding.

example: 
```javascript 
$bt.wait(2000)
```
**Parameters**

**ms**: `number`, Amount of milliseconds to wait for.

**Returns**: `Promise`

## page
```$bt.page()```

Returns the current page name, as identified by the url hash.

example: 
```javascript 
let page = $bt.page()
page.should.be.equal('home')
```
**Returns**: `string`


## get 
```$bt.get(query) ```

Get's the first element that matches the css-selector string and assigns it to the element variable. 

example: 
```javascript
$bt.get('div .myclass').element.innerText.should.be.equal('hello')

or 

let x = $bt.get('div .myclass')
x.innerText.should.be.equal('hello')
```
**Parameters**

**query**: `string`, A standard CSS selector string.

**Returns**: `Element`, The selected Element or null.
## getAll
```$bt.getAll(query)```

Get's all the elements that matches the css-selector string, and assigns them to the elements variable.

example: 
```javascript

$bt.getAll('.input')
$bt.elements[3].click()
```
**Parameters**

**query**: `string`, A standard CSS selector string.

**Returns**: `Array`, An array of elements.

## click
```$bt.click()```

Simulates a click event on an element.

example: 
```javascript
$bt.get('div .myclass').click()
```

## setValue

```$bt.setValue(value)``` 

Simulates an input event, and sets the value on the input.

example: 

```javascript
$bt.get('#pwInput').setValue('password')
```
**Parameters**

**value**: `string`, The value.

* * *
